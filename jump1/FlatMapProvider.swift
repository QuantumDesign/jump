//
//  FlatMapProvider.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 26.03.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import Foundation

class FlatMapProvider: MapProvider {
    init() {
        
    }
    
    subscript(pos : Int) -> CellProtocol {
        get {
            return SimpleCell(height:1, width: 1)
        }
        set{}
    }
}