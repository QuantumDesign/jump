//
//  UIImage+Utilities.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 05.04.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import UIKit

extension UIImage {
    func resizedImage(size:CGSize) -> UIImage! {
        let rect = CGRectMake(0,0,size.width, size.height);
        UIGraphicsBeginImageContext(rect.size );
        self.drawInRect(rect);
        let picture1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
//        NSData *imageData = UIImagePNGRepresentation(picture1);
//        UIImage *img=[UIImage imageWithData:imageData];
        return UIImage(CGImage: picture1.CGImage!)
    }
    
    public class func filledImage(color:UIColor, size:CGSize) -> UIImage {
        let rect = CGRectMake(0,0, size.width, size.height);
        UIGraphicsBeginImageContext( rect.size );
        color.set()
        UIRectFill(rect)
        let picture1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        let imageData = UIImagePNGRepresentation(picture1);
        let img=UIImage(data:imageData!);
        return img!;
    }
}