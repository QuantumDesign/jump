//
//  PlayerNode.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 03.04.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import SpriteKit

class PlayerNode: SKNode {
    let player = SKSpriteNode(imageNamed: "player")
    var startPosition: CGPoint = CGPointZero {
        didSet {
            player.position = startPosition
        }
    }
    
    override init() {
        super.init()
        createPlayer()
    }
    
    private func createPlayer() {
        player.position = startPosition
        player.physicsBody = SKPhysicsBody(rectangleOfSize: player.size)
        player.physicsBody?.dynamic = true;
        player.physicsBody?.mass = 60
        player.physicsBody?.linearDamping = 0
        player.physicsBody?.angularDamping = 1.0 // Could not rotate
        player.physicsBody?.friction = 0.0
        player.constraints = [SKConstraint.zRotation(SKRange(lowerLimit: 0.0, upperLimit: 0.0))]
        self.addChild(player)
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func jump() {
        if (player.physicsBody?.allContactedBodies().count >= 1) {
            player.physicsBody?.applyImpulse(CGVector(dx: 0.0, dy: 8000.0))
            NSLog("Jump");
        } else {
            NSLog("Cant Jump in air");
        }
    }

    var jetpackOn = false

    func jetpack (on:Bool) {
        jetpackOn = on
        NSLog("Jetpack %@", on)
    }

    func update() {
        player.physicsBody?.applyForce(CGVector(dx: 0.0, dy: (jetpackOn ? 20000.0 : 0.0)))
    }
    
    var isGameOver:Bool {
        get {
            let epsilon:Float = 1.0
            return abs(Float(player.position.x - startPosition.x)) > epsilon
        }
    }
}
