//
//  MapProvider.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 26.03.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import Foundation

protocol CellProtocol: class {
    init(height: Float, width:Float)
    var height:Float {get};
    var width:Float {get};
}

protocol MapProvider: class {
    subscript(pos : Int) -> CellProtocol { get };
}