//
//  MainScene.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 26.03.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import SpriteKit

class MainScene: SKScene, ScenePropertiesProvider {
    enum GameState {
        case Running
        case GameOver
    }
    weak var mapProvider : MapProvider?
    let cellCount = 16
    var heightInCells: Int
    var cellWidth:Float
    let player = PlayerNode()
    var groundNode : GroundNode?
    var backgroundNode : BackgroundNode?
    var worldPos:Float = 0.0;
    
    var currentGameState:GameState = .Running

    override init(size: CGSize) {
        cellWidth = Float(size.width) / Float(cellCount)
        heightInCells = Int(ceilf(Float(size.height) / cellWidth))
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        cellWidth = 1; // Hack to call super.init
        heightInCells = 1;
        super.init(coder: aDecoder)
        cellWidth = Float(size.width) / Float(cellCount)
        heightInCells = Int(ceilf(Float(size.height) / cellWidth))
    }

    override func didMoveToView(view: SKView) {
        physicsWorld.gravity = CGVectorMake(0, -1.0)
        player.startPosition = CGPoint(x: CGFloat(5.5*cellWidth), y:size.height * 0.5 )
        addChild(player)
        groundNode = GroundNode(mapProvider: self.mapProvider, scenePropertiesProvider: self)
        groundNode?.update(worldPos)
        addChild(groundNode!)
        backgroundNode = BackgroundNode(mapProvider: self.mapProvider, scenePropertiesProvider: self)
        addChild(backgroundNode!)
        currentGameState = .Running
    }
    
    
    var xMovementSpeed:Float = 0.07
    var lastUpdateTime:NSTimeInterval?
    
    override func update(currentTime: NSTimeInterval) {
        updateState()
        switch self.currentGameState {
        case .Running:
            updateRunningState(currentTime)
        case .GameOver:
            updateGameOver()
        default:
            NSLog("Game Over")
        }
        
    }
    
    func updateRunningState (currentTime: NSTimeInterval) {
        var timeDelta:Float = 0;
        if let lastTime = lastUpdateTime {
            timeDelta = Float(currentTime-lastTime)
        }
        let playerXShift = xMovementSpeed * timeDelta*cellWidth;
        player.update()
        worldPos += playerXShift
        groundNode?.update(worldPos)
        backgroundNode?.update(worldPos)
        
        lastUpdateTime = currentTime
    }
    
    func updateGameOver() {
        if self.childNodeWithName("GameOverLabel") != nil {return}
        let label = SKLabelNode(text: "Game Over")
        let centerPoint = CGPointMake(self.size.width/2, self.size.height/2)
        label.position = centerPoint
        label.zPosition = 100
        label.name = "GameOverLabel"
        label.fontColor = UIColor.blueColor()
        addChild(label)
    }
    
    func updateState() {
        if player.isGameOver {
            currentGameState = .GameOver
        }
    }
    
    func jump() {
        player.jump()
    }
    
    func jetpack (on:Bool) {
        player.jetpack(on)
        NSLog("Jetpack %@", on)
    }
}
