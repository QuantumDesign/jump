//
//  RealMapProvider.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 05.04.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import Foundation
extension Float {
    mutating func fitInLimits(min:Float, max: Float) {
        if self<min {
            self = min;
        }
        if self>max {
            self = max
        }
    }
}

class RealMap: MapProvider {
    let bunchSize = 16;
    var cells: [SimpleCell]
    
    init() {
        cells = Array()
    }
    
    subscript(pos : Int) -> CellProtocol {
        get {
            if cells.count>=pos {
                generateNextBunch()
            }
            return cells[(pos>=0 ? pos: 0)]
        }
    }
    
    func generateNextBunch() {
        let startingHeight:Float = getLastHeight()
        if cells.count > bunchSize {
            appendBunchOfHeightChanges(startingHeight)
        } else {
            appendBunchOfFlat(startingHeight)
        }
        
    }
    
    func getLastHeight() -> Float {
        let defaultStartingHeight:Float = 3.0
        return cells.count>0 ? (cells.last?.height)! : defaultStartingHeight
    }
    
    func appendBunchOfHeightChanges(startingHeight: Float) {
        var currentHeight = startingHeight
        for _ in 1...bunchSize {
            let randMax:UInt32 = 6
            let rand = arc4random_uniform(randMax);
            if rand == 0 {
                currentHeight -= 1
            } else if rand == randMax-1 {
                currentHeight += 1
            }
            currentHeight.fitInLimits(0.0, max: 10)
            cells.append(SimpleCell(height: currentHeight, width: 1.0));
        }
    }
    
    func appendBunchOfFlat (startingHeight: Float) {
        var currentHeight = startingHeight
        currentHeight.fitInLimits(0.0, max: 10)
        for _ in 1...bunchSize {
            cells.append(SimpleCell(height: currentHeight, width: 1.0));
        }
    }
    
}