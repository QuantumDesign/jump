//
//  SpikesMapProvider.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 27.03.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import Foundation

class SpikesMapProvider: MapProvider {
    init() {
        
    }
    
    subscript(pos : Int) -> CellProtocol {
        get {
            return SimpleCell(height:((pos%4==1) ? 2.5: 1.5), width: 1)
        }
        set{}
    }
}