//
//  MainSceneControllerViewController.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 26.03.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import UIKit

class MainSceneControllerViewController: UIViewController {
    var currentMapProvider :MapProvider?
    weak var scene:MainScene?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentMapProvider = RealMap()
        setupViewConnections()
        // Do any additional setup after loading the view.
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapped(_:)))
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
        tapRecognizer.numberOfTouchesRequired = 1
        longGesture.numberOfTouchesRequired = 1
        self.view.gestureRecognizers = [tapRecognizer, longGesture]
    }

    deinit {
        currentMapProvider = nil;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupViewConnections() {
        let view = self.view as! MainSceneView
        let scene = MainScene(size: view.bounds.size)
        scene.mapProvider = self.currentMapProvider;
        view.presentScene(scene)
        self.scene = scene
    }
    
    func tapped(sender: UITapGestureRecognizer)  {
        if sender.state == .Ended {
            if scene?.currentGameState == MainScene.GameState.GameOver {
              self.dismissViewControllerAnimated(true, completion: nil)
            } else {
            if sender.numberOfTouches()==1 {
                // Jump
                dispatch_async(dispatch_get_main_queue(), {
                    let view = self.view as! MainSceneView
                    let scene = view.scene as! MainScene
                    scene.jump()
                })
            }
            }
        }
    }
    
    func longPressed(sender: UITapGestureRecognizer) {
        dispatch_async(dispatch_get_main_queue(), {
            let view = self.view as! MainSceneView
            let scene = view.scene as! MainScene
            switch sender.state {
            case .Began:
                scene.jetpack(true)
            case .Changed:
                break
            default:
                scene.jetpack(false)
            }
        })
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
