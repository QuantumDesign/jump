//
//  BackgroundNode.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 04.04.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import SpriteKit

class BackgroundNode: SKShapeNode {
    weak var mapProvider : MapProvider?
    var backgroundNode = SKSpriteNode()
    weak var sceneProperties : ScenePropertiesProvider?
    
    convenience init(mapProvider: MapProvider?, scenePropertiesProvider: ScenePropertiesProvider?) {
        self.init()
        self.mapProvider = mapProvider
        self.sceneProperties = scenePropertiesProvider
        self.position = CGPointZero
        self.zPosition = -100
        prepareNode()
    }
    
    func prepareNode() {
        var height: Float = 0.0
        var width: Float = 0.0
        var cellHeight:Float = 0.0
        var cellWidth: Float = 0.0
        if let sc = sceneProperties {
            height = sc.cellWidth * Float(sc.heightInCells+2)
            width = sc.cellWidth * Float(sc.cellCount+2)
            cellWidth = sc.cellWidth
            cellHeight = sc.cellWidth
        }
        let spriteSize = CGSize(width: CGFloat(width), height: CGFloat(height))
        let cellSize = CGSize(width: CGFloat(cellWidth), height: CGFloat(cellHeight))
        let img = UIImage(named: "cell")?.resizedImage(cellSize)
        let col = UIColor(patternImage:img!);
        let img2 = UIImage.filledImage(col, size:spriteSize)
        let text = SKTexture(image:img2)
        
        backgroundNode = SKSpriteNode(texture: text, size: spriteSize)
        backgroundNode.anchorPoint = CGPointMake(0.0, 0.0)
        backgroundNode.position = CGPoint (x: Double(-cellWidth), y: Double(5.0-cellHeight))
        addChild(backgroundNode)
    }
    
    func update(worldPos:Float) {
        let indexOfWorldPosition:Int = Int(floor(worldPos))
        let smallShift = worldPos - Float(indexOfWorldPosition)
        var cellWidth: Float = 0.0
        if let sc = sceneProperties {
            cellWidth = sc.cellWidth
        }
        backgroundNode.position = CGPoint(x: CGFloat((-smallShift)*cellWidth), y: backgroundNode.position.y)
    }
}
