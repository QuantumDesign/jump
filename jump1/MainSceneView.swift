//
//  MainSceneView.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 26.03.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import UIKit
import SpriteKit

class MainSceneView: SKView {
    var mainScene : MainScene?
    
    var sizeOfCell : Float = 40;
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        self.showsFPS = true
    }
}
