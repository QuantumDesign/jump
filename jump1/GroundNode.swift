//
//  GroundNode.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 03.04.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import SpriteKit
protocol ScenePropertiesProvider : class {
    var cellCount: Int { get }
    var cellWidth: Float { get }
    var heightInCells: Int { get }
}

class GroundNode: SKNode {
    weak var mapProvider : MapProvider?
    var groundNode = SKShapeNode()
    weak var sceneProperties : ScenePropertiesProvider?

    convenience init(mapProvider: MapProvider?, scenePropertiesProvider: ScenePropertiesProvider?) {
        self.init()
        self.mapProvider = mapProvider
        self.sceneProperties = scenePropertiesProvider
        self.position = CGPointZero
        update(0.0)
    }
    
    func update(worldPos:Float) {
        let path = CGPathCreateMutable()
        groundNode.removeFromParent()
        CGPathMoveToPoint(path, nil, 0.0, 0.0)
        let indexOfWorldPosition:Int = Int(floor(worldPos))
        let smallShift = worldPos - Float(indexOfWorldPosition)
        
        var cellCount: Int = 0
        var cellWidth: Float = 0
        if let scProp = sceneProperties {
            cellCount = scProp.cellCount
            cellWidth = scProp.cellWidth
        }

        if let map = mapProvider {
            for index in -1...cellCount {
                let previousCell = map[indexOfWorldPosition+index-1]
                let cell = map[indexOfWorldPosition+index]
                let screenHeight = cellWidth * cell.height
                if previousCell.height != cell.height {
                    CGPathAddLineToPoint(path, nil,
                                         CGFloat((Float(index)-smallShift)*cellWidth),
                                         CGFloat(screenHeight))
                }
                CGPathAddLineToPoint(path, nil,
                                     CGFloat((Float(index+1)-smallShift)*cellWidth),
                                     CGFloat(screenHeight))
                
            }
        }
        groundNode = SKShapeNode(path: path);
        groundNode.lineWidth=2
        groundNode.strokeColor = UIColor.brownColor()
        groundNode.physicsBody = SKPhysicsBody(edgeChainFromPath: path)
        
        addChild(groundNode)
    }
    

}
