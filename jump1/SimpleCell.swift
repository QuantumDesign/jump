//
//  SimpleCell.swift
//  jump1
//
//  Created by Rostyslav Drutskyy on 27.03.16.
//  Copyright © 2016 Rostyslav Drutskyy. All rights reserved.
//

import Foundation
class SimpleCell: CellProtocol {
    var height: Float = 0
    var width: Float
    
    required init(height: Float, width:Float) {
        self.height = height
        self.width = width
    }
}
